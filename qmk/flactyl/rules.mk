SERIAL_DRIVER = usart

#debug
# CONSOLE_ENABLE = no         # debug, use with #include "print.h", uprint("\n"); and uprintf("data%s\n",v); with qmk console

OPT_DEFS += -DKEYBOARD_REVISION=\"$(shell git -C keyboards/${KEYBOARD} rev-parse --short HEAD)\"
OPT_DEFS += -DKEYBOARD_BRANCH=\"$(shell git -C keyboards/${KEYBOARD} rev-parse --abbrev-ref HEAD)\"
OPT_DEFS += -DKEYBOARD_DATE=\"$(shell git -C keyboards/${KEYBOARD} log -1 --pretty="format:%ad%n" --date="format:%Y%m%d")\"
