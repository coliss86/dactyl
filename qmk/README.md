# Firmware

[QMK](https://github.com/qmk/qmk_firmware) is used to setup the micro-controller as a keyboard.

First, setup the [build environment setup](https://docs.qmk.fm/#/newbs_getting_started).

Then, add the source of this project to the QMK directory:
```sh
cd qmk_firmware/keyboards
git clone https://gitlab.com/coliss86/dactyl
```

or clone it in an external directory:
```sh
git clone https://gitlab.com/coliss86/dactyl
cd qmk_firmware/keyboards
ln -s ../../dactyl/qmk dactyl
```

To update `qmk_firmware`, run:
```sh
cd qmk_firmware
git fetch -a
git checkout 0.24.8
make git-submodule
```

run the following to migrate the configuration:
```sh
qmk migrate -kb dactyl
```

## Format the code

```sh
clang-format -i keymaps/default/keymap.c
```

## Flash the firmware

-  First, put the microcontroller in dfu mode, either use:
  * **Keycode in layout**: Press the key mapped to `RESET`.
  * **Physical**: To put the mcu in dfu-mode, press *BOOT0*, then *NRST*, release it and release *BOOT0*

- then compile the firmware:

```sh
qmk compile -kb dactyl/dactyl -km default
# or
qmk compile -kb dactyl/flactyl -km default
```

- next, flash the keyboard:

```sh
qmk flash -kb dactyl/dactyl -km default
# or
qmk flash -kb dactyl/flactyl -km default
```
