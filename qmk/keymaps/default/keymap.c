/*
 * Copyright 2022 Coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

#include "version.h"
#include "keymap_french.h"
#include "sendstring_french.h"
#include "print.h"
#include "os_detection.h"

#define DELAY_TAP_CODE 10

enum layers {
    _COLEMAK_MACOS = 0,
    _PC,
    _GAME_DOOM,
    _GAME_FPS,
    _FUNC,
    _UPPER,
    _NAV,
};

// Macro Declarations
// clang-format off
enum custom_keycodes {
    EMPTY = SAFE_RANGE, EMPTY2,
    A_GRAVE, A_CIRC, A_GR_CI, E_ACUTE, E_GRAVE, E_CIRC, E_TREMA, E_ACCENT, I_ACCENT, I_CIRC, I_TREMA, U_GRAVE, U_CIRC, U_TREMA, U_GR_CI, U_ACCENT, O_CIRC, O_ACCENT, C_CEDI, // accent
    MICRO, EURO, DOLLAR, // symbols
    DOT, COMMA, SCLN_CL, SCLN, COLON, PIPE, PIP_BSL, SD_QUOT, SLASH, MINS, UNDRSCR, EQUAL, PLUS, TILDE, ASTR, DGRAVE, DQUOTE, SQUOTE, EXCLAM, AT, DASH, PERCEN, CIRC, AMPER, ASTER, // punctuation
    SLCBR, LCBR, RCBR, LBRC, RBRC, LPRN, RPRN, R_BR, L_BR, // parenthesis
    ALT_TAB, GRA_ESC, HOME, END, C_A_SUP, MUTE_BRG, RGB_HSV, // utils
    MAGIC_T, MAGIC_G, MAGIC_BC, MAGIC_W, // supercharged keys
    MAKE, VERSION, INVALID, // function
    PC, MAC, G_FPS, G_DOOM, // layers
    NUM_0, NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7, NUM_8, NUM_9, // numbers
};
// clang-format on

// Tap Dance declarations
enum tap_dance {
    SMILEY_HAPPY,
};
#define TD_HAPP TD(SMILEY_HAPPY)

#define LR_FUNC MO(_FUNC)
#define LR_NAV MO(_NAV)
#define UP_SLSH LT(_UPPER, EMPTY) // LT() is used instead MT() to enter in process_record_user()
#define ALT_LBR LT(0, EMPTY2)
#define GUI_LBR LT(0, EMPTY)
#define SP_LSFT MT(MOD_LSFT, KC_SPC)
#define SP_RSFT MT(MOD_RSFT, KC_SPC)
#define UPPER OSL(_UPPER)
#define xxxxxxx INVALID

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_COLEMAK_MACOS] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    GRA_ESC, NUM_1,   NUM_2,   NUM_3,   NUM_4,   NUM_5,   MUTE_BRG,                     xxxxxxx, NUM_6,   NUM_7,   NUM_8,   NUM_9,   NUM_0,   TD_HAPP,
    KC_TAB,  FR_Q,    MAGIC_W, FR_F,    FR_P,    FR_B,    xxxxxxx,                      QK_LEAD, FR_J,    FR_L,    FR_U,    SCLN_CL, FR_Y,    EQUAL,
    QK_LEAD, FR_A,    FR_R,    FR_S,    MAGIC_T, MAGIC_G, MAGIC_BC,                     xxxxxxx, FR_M,    FR_N,    FR_E,    FR_I,    FR_O,    PIP_BSL,
    LR_FUNC, FR_Z,    FR_X,    FR_C,    FR_D,    FR_V,                                           FR_K,    FR_H,    COMMA,   DOT,     KC_UP,   KC_RGHT,
                      xxxxxxx, xxxxxxx,                                                                            KC_LEFT, KC_DOWN,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     KC_LGUI, SP_LSFT, KC_ENT,    MAGIC_BC,SP_RSFT, UPPER,
                                                              KC_LCTL ,ALT_LBR,   SLASH,   LR_NAV
),

[_UPPER] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    DGRAVE,  EXCLAM,  AT,      DASH,    EURO,    PERCEN,  xxxxxxx,                      xxxxxxx, CIRC,    AMPER,   ASTR,    LPRN,    RPRN,    MAKE,
    xxxxxxx, xxxxxxx, xxxxxxx, DOLLAR,  SLCBR,   RCBR,    xxxxxxx,                      xxxxxxx, FR_DEG,  xxxxxxx, U_GR_CI, SCLN,    xxxxxxx, MINS,
    xxxxxxx, A_GR_CI, UNDRSCR, MINS,    SQUOTE,  DQUOTE,  KC_BSPC,                      xxxxxxx, TILDE,   E_GRAVE, E_ACUTE, I_CIRC,  O_CIRC,  _______,
    xxxxxxx, xxxxxxx, xxxxxxx, C_CEDI,  SLASH,   ASTR,                                           MICRO,   xxxxxxx, E_CIRC,  _______, KC_DOWN, KC_LEFT,
                      xxxxxxx, xxxxxxx,                                                                            xxxxxxx, xxxxxxx,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     _______, _______, _______,   _______, _______, _______,
                                                              _______, LPRN,      RPRN,    _______
),

[_PC] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______,                                        _______, _______, _______, _______, _______, _______,
                      _______, _______,                                                                            _______, _______,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     KC_LALT, _______, _______,   _______, _______, _______,
                                                              _______, GUI_LBR,   _______, _______
),

[_GAME_DOOM] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, FR_U,    _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______,                                        _______, _______, _______, _______, _______, _______,
                      _______, _______,                                                                            _______, _______,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     FR_I,    KC_SPC,  _______,   _______, _______, _______,
                                                              FR_E,    _______,   _______, _______
),

[_GAME_FPS] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    KC_ESC,  FR_1,    FR_2,    FR_3,    FR_4,    FR_5,    KC_F6,                        xxxxxxx, FR_6,    FR_7,    FR_8,    FR_9,    FR_0,    xxxxxxx,
    KC_TAB,  FR_Y,    FR_A,    FR_Z,    FR_E,    FR_R,    FR_T,                         xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
    xxxxxxx, KC_LSFT, FR_Q,    FR_S,    FR_D,    FR_F,    FR_N,                         xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
    _______, FR_G,    FR_X,    FR_C,    FR_V,    FR_B,                                           xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, _______, _______,
                      xxxxxxx, FR_W,                                                                               _______, _______,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     _______, KC_SPC,  _______,   xxxxxxx, xxxxxxx, _______,
                                                              KC_LCTL, FR_I,      xxxxxxx, _______
),

[_FUNC] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    MAKE,    KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   xxxxxxx,                      MAKE,    KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,
    VERSION, xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, G_FPS,   xxxxxxx,                      xxxxxxx, xxxxxxx, PC,      xxxxxxx, xxxxxxx, RGB_HUI, RGB_HUD,
    xxxxxxx, xxxxxxx, RGB_M_R, RGB_M_SW,RGB_TOG, G_FPS,   xxxxxxx,                      xxxxxxx, MAC,     xxxxxxx, xxxxxxx, xxxxxxx, RGB_SAI, RGB_SAD,
    xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx, G_DOOM,  xxxxxxx,                                        RGB_M_K, xxxxxxx, xxxxxxx, xxxxxxx, RGB_VAI, RGB_VAD,
                      xxxxxxx, PC,                                                                                 xxxxxxx, xxxxxxx,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     xxxxxxx, xxxxxxx, xxxxxxx,   xxxxxxx, xxxxxxx, xxxxxxx,
                                                              C_A_SUP, xxxxxxx,   xxxxxxx, xxxxxxx
),

[_NAV] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    _______, _______, _______, _______, _______, _______, _______,                      xxxxxxx, xxxxxxx, xxxxxxx, KC_PGUP, xxxxxxx, xxxxxxx, xxxxxxx,
    _______, _______, _______, _______, _______, _______, _______,                      xxxxxxx, xxxxxxx, xxxxxxx, KC_UP,   xxxxxxx, xxxxxxx, xxxxxxx,
    _______, _______, _______, _______, _______, _______, _______,                      xxxxxxx, HOME,    KC_LEFT, KC_DOWN, KC_RGHT, END,     xxxxxxx,
    _______, _______, _______, _______, _______, _______,                                        xxxxxxx, KC_PGUP, KC_PGDN, xxxxxxx, xxxxxxx, xxxxxxx,
                      _______, _______,                                                                            xxxxxxx, xxxxxxx,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     _______, _______, _______,   _______, _______, _______,
                                                              _______, _______,   _______, _______
),

/*
[_TEMPLATE] = LAYOUT_5x7(
// ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐                    ┌────────┬────────┬────────┬────────┬────────┬────────┬────────┐
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______,                      _______, _______, _______, _______, _______, _______, _______,
    _______, _______, _______, _______, _______, _______,                                        _______, _______, _______, _______, _______, _______,
                      _______, _______,                                                                            _______, _______,
//                                                  ┌────────┬────────┬───────┐  ┌────────┬────────┬───────┐
                                                     _______, _______, _______,   _______, _______, _______,
                                                              _______, _______,   _______, _______
),
*/
};
// clang-format on

/**************** Led effects ****************/

#if defined(RGBLIGHT_ENABLE)

bool              is_notify_color_layer = false;
uint8_t           notif_color[2];
uint8_t           flash_nb                 = 0;
bool              led_fade_off_in_progress = false;
rgblight_config_t previous_rgblight;

void rgblight_show_solid_color_noeeprom(uint8_t h, uint8_t s, uint8_t v) {
    rgblight_enable_noeeprom();
    rgblight_mode_noeeprom(RGBLIGHT_MODE_STATIC_LIGHT);
    rgblight_sethsv_noeeprom(h, s, v);
}

void store_rgb_mode(void) {
    previous_rgblight.mode  = rgblight_get_mode();
    previous_rgblight.hue   = rgblight_get_hue();
    previous_rgblight.sat   = rgblight_get_sat();
    previous_rgblight.val   = rgblight_get_val();
    previous_rgblight.speed = rgblight_get_speed();
}

void restore_previous_rgb_mode(void) {
    rgblight_mode_noeeprom(previous_rgblight.mode);
#    ifdef RGBLIGHT_USE_TIMER
    // don't restart the animation to continue the rainbow mood at the previous hue
    // taken from rgblight_mode_eeprom_helper() in quantum/rgblight/rgblight.c
    animation_status.restart = false;
#    endif
    rgblight_sethsv_noeeprom(previous_rgblight.hue, previous_rgblight.sat, previous_rgblight.val);
    rgblight_set_speed_noeeprom(previous_rgblight.speed);
}

uint32_t led_off_callback(uint32_t trigger_time, void *cb_arg) {
    rgblight_sethsv_noeeprom(HSV_OFF);
    return 0;
}

void delay_led_off(void) {
    defer_exec(2000, led_off_callback, NULL);
}

uint32_t led_restore_mode_callback(uint32_t trigger_time, void *cb_arg) {
    restore_previous_rgb_mode();
    return 0;
}

uint32_t led_fade_off_callback(uint32_t trigger_time, void *cb_arg) {
    rgblight_decrease_val_noeeprom();
    if (rgblight_get_val() > 0) {
        return 35;
    } else {
        led_fade_off_in_progress = false;
        return 0;
    }
}

void delay_led_fade_off(void) {
    if (led_fade_off_in_progress) {
        return;
    }
    led_fade_off_in_progress = true;
    defer_exec(3000, led_fade_off_callback, NULL);
}

void led_fade_off(void) {
    if (led_fade_off_in_progress) {
        return;
    }
    led_fade_off_in_progress = true;
    defer_exec(1, led_fade_off_callback, NULL);
}

uint32_t flash_led_callback(uint32_t trigger_time, void *cb_arg) {
    rgblight_sethsv_noeeprom(notif_color[0], notif_color[1], RGBLIGHT_DEFAULT_VAL);
    flash_nb--;
    if (flash_nb > 0) {
        defer_exec(LED_FLASH_INTERVAL, led_off_callback, NULL);
        return LED_FLASH_INTERVAL * 2;
    } else {
        defer_exec(LED_FLASH_INTERVAL, led_restore_mode_callback, NULL);
        return 0;
    }
}

void notify_color(uint8_t hue, uint8_t sat, uint8_t val) {
    if (flash_nb <= 0) { // poor man lock
        store_rgb_mode();
        rgblight_mode_noeeprom(RGBLIGHT_MODE_STATIC_LIGHT);

        flash_nb            = 3;
        uint8_t color_cmd[] = {hue, sat, val};
        for (uint8_t i = 0; i < sizeof notif_color; i++) {
            notif_color[i] = color_cmd[i];
        }
        defer_exec(1, flash_led_callback, NULL);
    }
}

void notify_ko(void) {
    notify_color(HSV_RED);
}

void notify_warning(void) {
    notify_color(200, 255, 255);
}

#else
void notify_ko(void) {}
void notify_warning(void) {}
#endif

/**************** Switch layer ****************/

// a boolean is needed in addition of the current layer because some action are not on the based layer
static bool is_mac  = true;
static bool is_game = false;

#if defined(RGBLIGHT_ENABLE)
#    define COLOR_MAC HSV_WHITE
#    define COLOR_PC HSV_BLUE
#    define COLOR_GAME HSV_PURPLE
#endif

void notify_color_layer(void) {
#if defined(RGBLIGHT_ENABLE)
    if (is_notify_color_layer) {
        if (is_game) {
            notify_color(COLOR_GAME);
        } else if (IS_LAYER_ON(_PC)) {
            notify_color(COLOR_PC);
        } else if (is_mac) {
            notify_color(COLOR_MAC);
        }
    }
#endif
}

void switch_layer_to_pc(void) {
    is_mac  = false;
    is_game = false;
    layer_clear();
    layer_move(_COLEMAK_MACOS);
    layer_on(_PC);
    notify_color_layer();
}

void switch_layer_to_mac(void) {
    is_mac  = true;
    is_game = false;
    layer_clear();
    layer_move(_COLEMAK_MACOS);
    notify_color_layer();
}

void switch_layer_to_game(uint8_t layer, bool pc) {
    layer_clear();
    layer_move(_COLEMAK_MACOS);
    layer_on(layer);
    is_mac  = !pc;
    is_game = true;
    if (pc) {
        layer_on(_PC);
    }
    notify_color_layer();
}

/**************** Key definition ****************/

#define SS_FR_C_CEDI SS_TAP(X_9)                                                 // ç
#define SS_FR_C_CEDI_MACOS_CAPITAL SS_ALGR(SS_TAP(X_9))                          // Ç = capital c cedil
#define SS_FR_E_ACUTE SS_TAP(X_2)                                                // é
#define SS_FR_E_ACUTE_CAPITAL_MAC_OS SS_TAP(X_CAPS) SS_FR_E_ACUTE SS_TAP(X_CAPS) // É = capital e acute
#define SS_FR_E_GRAVE SS_TAP(X_7)                                                // è
#define SS_FR_A_GRAVE SS_TAP(X_0)                                                // à
#define SS_FR_A_GRAVE_MACOS_CAPITAL SS_TAP(X_BSLS) SS_LSFT(SS_TAP(X_Q))          // À = capital a grave
#define SS_FR_GRAVE SS_ALGR(SS_TAP(X_7))                                         // `
#define SS_FR_GRAVE_MACOS SS_TAP(X_NUHS) SS_TAP(X_SPACE)                         // `
#define SS_FR_TILD SS_ALGR(SS_TAP(X_2))                                          // ~
#define SS_FR_TILD_MACOS SS_ALGR(SS_TAP(X_N))                                    // ~
#define SS_FR_A_CIRC SS_TAP(X_LBRC) SS_TAP(X_Q)                                  // â
#define SS_FR_E_CIRC SS_TAP(X_LBRC) SS_TAP(X_E)                                  // ê
#define SS_FR_E_CIRC_CAPITAL_MAC_OS SS_TAP(X_LBRC) SS_LSFT(SS_TAP(X_E))          // Ê = capital e circ
#define SS_FR_I_CIRC SS_TAP(X_LBRC) SS_TAP(X_I)                                  // î
#define SS_FR_O_CIRC SS_TAP(X_LBRC) SS_TAP(X_O)                                  // ô
#define SS_FR_U_CIRC SS_TAP(X_LBRC) SS_TAP(X_U)                                  // û
#define SS_FR_U_GRAVE SS_TAP(X_QUOT)                                             // ù
#define SS_FR_U_TREMA SS_LSFT(SS_TAP(X_LBRC)) SS_TAP(X_U)                        // ü
#define SS_FR_E_TREMA SS_LSFT(SS_TAP(X_LBRC)) SS_TAP(X_E)                        // ë
#define SS_FR_I_TREMA SS_LSFT(SS_TAP(X_LBRC)) SS_TAP(X_I)                        // ï
#define SS_FR_O_TREMA SS_LSFT(SS_TAP(X_LBRC)) SS_TAP(X_O)                        // ö
#define SS_FR_MICRO SS_LSFT(SS_TAP(X_NUHS))                                      // µ
#define SS_FR_MICRO_MACOS SS_LALT(SS_TAP(X_SCLN))                                // µ
#define SS_FR_LPRN SS_TAP(X_5)                                                   // (
#define SS_FR_RPRN SS_TAP(X_MINS)                                                // (
#define SS_FR_LCBR SS_ALGR(SS_TAP(X_4))                                          // {
#define SS_FR_LCBR_MACOS SS_ALGR(SS_TAP(X_5))                                    // {
#define SS_FR_RCBR SS_ALGR(SS_TAP(X_EQL))                                        // }
#define SS_FR_RCBR_MACOS SS_ALGR(SS_TAP(X_MINS))                                 // }
#define SS_FR_LBRC SS_ALGR(SS_TAP(X_5))                                          // [
#define SS_FR_LBRC_MACOS SS_LSFT(SS_ALGR(SS_TAP(X_5)))                           // [
#define SS_FR_RBRC SS_ALGR(SS_TAP(X_MINS))                                       // ]
#define SS_FR_RBRC_MACOS SS_LSFT(SS_ALGR(SS_TAP(X_MINS)))                        // ]
#define SS_FR_EURO SS_ALGR(SS_TAP(X_E))                                          // €
#define SS_FR_EURO_MACOS SS_LALT(SS_TAP(X_RBRC))                                 // €
#define SS_FR_EQL_MACOS SS_TAP(X_SLSH)                                           // =
#define SS_FR_EQL SS_TAP(X_EQL)                                                  // =
#define SS_FR_PLUS_MACOS SS_LSFT(SS_TAP(X_SLSH))                                 // +
#define SS_FR_PLUS SS_LSFT(SS_TAP(X_EQL))                                        // +
#define SS_FR_MINUS_MACOS SS_TAP(X_EQL)                                          // -
#define SS_FR_MINUS SS_TAP(X_6)                                                  // -
#define SS_FR_EXCLAM SS_TAP(X_SLSH)                                              // !
#define SS_FR_EXCLAM_MACOS SS_TAP(X_8)                                           // !
#define SS_FR_AT SS_ALGR(SS_TAP(X_0))                                            // @
#define SS_FR_AT_MACOS SS_TAP(X_NUBS)                                            // @
#define SS_FR_DASH SS_ALGR(SS_TAP(X_3))                                          // #
#define SS_FR_DASH_MACOS SS_LSFT(SS_TAP(X_NUBS))                                 // #
#define SS_FR_DOLLAR SS_TAP(X_RBRC)                                              // $
#define SS_FR_PERCENT SS_LSFT(SS_TAP(X_QUOT))                                    // %
#define SS_FR_CIRCUMFLUX SS_TAP(X_LBRC) SS_DELAY(DELAY_TAP_CODE) SS_TAP(X_SPACE) // ^
#define SS_FR_AMPERSAND SS_TAP(X_1)                                              // &
#define SS_UNDERSCOR_MACOS SS_LSFT(SS_TAP(X_EQL))                                // _
#define SS_UNDERSCOR SS_TAP(X_8)                                                 // _
#define SS_PIPE_MACOS SS_LSFT(SS_ALGR(SS_TAP(X_L)))                              // |
#define SS_PIPE SS_ALGR(SS_TAP(X_6))                                             // |
#define SS_BACKSLASH_MACOS SS_LSFT(SS_ALGR(SS_TAP(X_DOT)))                       // backslash
#define SS_BACKSLASH SS_ALGR(SS_TAP(X_8))                                        // backslash
#define SS_ASTERIX SS_TAP(X_NUHS)                                                // *
#define SS_ASTERIX_MACOS SS_LSFT(SS_TAP(X_RBRC))                                 // *
#define SS_DQUOTE SS_TAP(X_3)                                                    // "
#define SS_SQUOTE SS_TAP(X_4)                                                    // '
#define MOD_MASK_LALT MOD_BIT(KC_LALT)

bool mod_gui_mac_or_ctrl_pc(uint8_t mod_state) {
    return (is_mac && mod_state & MOD_MASK_GUI) || (!is_mac && mod_state & MOD_MASK_CTRL);
}

void key_ss(keyrecord_t *record, uint8_t mod_state, const char *str) {
    if (record->event.pressed) {
        unregister_mods(mod_state);
        wait_ms(DELAY_TAP_CODE);
        send_string(str);
        wait_ms(DELAY_TAP_CODE);
        register_mods(mod_state);
    }
}

void key_ss_pc_macos(keyrecord_t *record, uint8_t mod_state, const char *str_pc, const char *str_macos) {
    if (record->event.pressed) {
        unregister_mods(mod_state);
        wait_ms(DELAY_TAP_CODE);
        if (is_mac) {
            send_string(str_macos);
        } else {
            send_string(str_pc);
        }
        wait_ms(DELAY_TAP_CODE);
        register_mods(mod_state);
    }
}

void key_shift_pc_macos(keyrecord_t *record, uint8_t mod_state, const char *str_pc, const char *str_macos, const char *str_shift_pc, const char *str_shift_macos) {
    if (record->event.pressed) {
        if (mod_state & MOD_MASK_SHIFT) {
            unregister_mods(mod_state);
            wait_ms(DELAY_TAP_CODE);
            if (strlen(str_shift_macos) != 0 && is_mac) {
                send_string_with_delay(str_shift_macos, DELAY_TAP_CODE);
            } else {
                send_string_with_delay(str_shift_pc, DELAY_TAP_CODE);
            }
            wait_ms(DELAY_TAP_CODE);
            register_mods(mod_state);
        } else {
            if (strlen(str_macos) != 0 && is_mac) {
                send_string_with_delay(str_macos, DELAY_TAP_CODE);
            } else {
                send_string_with_delay(str_pc, DELAY_TAP_CODE);
            }
        }
    }
}

// variation of key_shift_pc_macos() which don't override CMD + SHIFT
void key_shift_number_pc_macos(keyrecord_t *record, uint8_t mod_state, const char *str, const char *str_shift_pc, const char *str_shift_macos) {
    if (record->event.pressed) {
        if (mod_state & MOD_MASK_SHIFT && !mod_gui_mac_or_ctrl_pc(mod_state)) {
            unregister_mods(mod_state);
            wait_ms(DELAY_TAP_CODE);
            if (strlen(str_shift_macos) != 0 && is_mac) {
                send_string_with_delay(str_shift_macos, DELAY_TAP_CODE);
            } else {
                send_string_with_delay(str_shift_pc, DELAY_TAP_CODE);
            }
            wait_ms(DELAY_TAP_CODE);
            register_mods(mod_state);
        } else {
            register_mods(MOD_MASK_SHIFT);
            SEND_STRING_DELAY(str, DELAY_TAP_CODE);
            unregister_mods(MOD_MASK_SHIFT);
        }
    }
}

void key_accent_capital(keyrecord_t *record, uint8_t mod_state, const char *accent, const char *accent_capital_mac) {
    if (record->event.pressed) {
        if (is_mac && mod_state & MOD_MASK_SHIFT) { // accent capital
            unregister_mods(MOD_MASK_SHIFT);
            wait_ms(DELAY_TAP_CODE);
            SEND_STRING(accent_capital_mac);
            wait_ms(DELAY_TAP_CODE);
            register_mods(mod_state);
        } else {
            SEND_STRING(accent);
        }
    }
}

void key_double_accent(keyrecord_t *record, uint8_t mod_state, const char *accent1, const char *accent2) {
    if (record->event.pressed) {
        if (mod_state & MOD_MASK_SHIFT) {
            unregister_mods(MOD_MASK_SHIFT);
            wait_ms(DELAY_TAP_CODE);
            SEND_STRING(accent1);
            wait_ms(DELAY_TAP_CODE);
            register_mods(mod_state);
        } else {
            SEND_STRING(accent2);
        }
    }
}

#define KEY_BASIC(SS)              \
    key_ss(record, mod_state, SS); \
    return false;

#define KEY_NUMBER_PC_MACOS(NUM, SS_PC, SS_MACOS)                               \
    key_shift_number_pc_macos(record, mod_state, SS_TAP(NUM), SS_PC, SS_MACOS); \
    break;

#define KEY_NUMBER(NUM, SS_SHIFT)                                            \
    key_shift_number_pc_macos(record, mod_state, SS_TAP(NUM), SS_SHIFT, ""); \
    break;

#define KEY_SHIFT(SS, SS_SHIFT)                                  \
    key_shift_pc_macos(record, mod_state, SS, "", SS_SHIFT, ""); \
    break;

#define KEY_SHIFT_PC_MACOS(SS, SS_SHIFT_PC, SS_SHIFT_MACOS)                     \
    key_shift_pc_macos(record, mod_state, SS, "", SS_SHIFT_PC, SS_SHIFT_MACOS); \
    break;

#define KEY_ACCENT_CAPITAL(SS, SS_CAPITAL_MACOS)                 \
    key_accent_capital(record, mod_state, SS, SS_CAPITAL_MACOS); \
    return false;

#define KEY_DOUBLE_ACCENT(SS_ACCENT1, SS_ACCENT2)                  \
    key_double_accent(record, mod_state, SS_ACCENT1, SS_ACCENT2); \
    return false;

#define KEY_ACCENT_CIRC_TREMA(SS_TREMA, SS_CIRC) \
    if (record->event.pressed) {                 \
        if (mod_state & MOD_MASK_SHIFT) {        \
            send_string(SS_TREMA);               \
            register_mods(mod_state);            \
        } else {                                 \
            send_string(SS_CIRC);                \
        }                                        \
    }                                            \
    return false;

#define KEY_PC_MACOS_WITH_SHIFT(SS_PC, SS_MACOS) \
    if (record->event.pressed) {      \
        if (is_mac) {                 \
            SEND_STRING(SS_MACOS);    \
        } else {                      \
            SEND_STRING(SS_PC);       \
        }                             \
    }                                 \
    return false;

#define KEY_PC_MACOS(SS_PC, SS_MACOS)                    \
    key_ss_pc_macos(record, mod_state, SS_PC, SS_MACOS); \
    return false;

// Initialize a boolean variable that keeps track
// of the tab key status: registered or not?
static bool tabkey_registered;

uint8_t  shift_registered = 0;
uint16_t shift_timer      = 0;
bool     previous_key_number = false;
bool     within_number       = false;
uint16_t dollar_timer        = 0;
bool     lcbr_registered     = false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    // Store the current modifier state in the variable for later reference
    uint8_t mod_state = get_mods();

    if (keycode != KC_BSPC && keycode != MAGIC_BC && record->event.pressed && (mod_state & MOD_MASK_SHIFT)) {
        shift_registered++;
    }
    if (keycode == SP_LSFT || keycode == SP_RSFT) {
        shift_registered = record->event.pressed;
        if (!record->event.pressed) { // key released
            shift_timer = timer_read();
        }
    }

    // Determine whether the key is a number.
    previous_key_number = within_number;
    switch (keycode) {
        case NUM_0 ... NUM_9:
            within_number = true;
            break;
        case MAGIC_W:
            within_number = previous_key_number;
            break;

        default:
            within_number = false;
    }

    // ----------------
    switch (keycode) {
        // ----------------
        // number row
        case NUM_1:
            KEY_NUMBER_PC_MACOS(X_1, SS_FR_EXCLAM, SS_FR_EXCLAM_MACOS) // 1 !
        case NUM_2:
            KEY_NUMBER_PC_MACOS(X_2, SS_FR_AT, SS_FR_AT_MACOS) // 2 @
        case NUM_3:
            KEY_NUMBER_PC_MACOS(X_3, SS_FR_DASH, SS_FR_DASH_MACOS) // 3 #
        case NUM_4:
            KEY_NUMBER(X_4, SS_FR_DOLLAR) // 4 $
        case NUM_5:
            KEY_NUMBER(X_5, SS_FR_PERCENT) // 5 %
        case NUM_6:
            KEY_NUMBER(X_6, SS_FR_CIRCUMFLUX) // 6 ^
        case NUM_7:
            KEY_NUMBER(X_7, SS_FR_AMPERSAND) // 7 &
        case NUM_8:
            KEY_NUMBER_PC_MACOS(X_8, SS_ASTERIX, SS_ASTERIX_MACOS) // 8 *
        case NUM_9:
            KEY_NUMBER(X_9, SS_FR_LPRN) // 9 (
        case NUM_0:
            if (record->event.pressed) {
                if (mod_gui_mac_or_ctrl_pc(mod_state)) {
                    tap_code(KC_0); // override : cmd + 0 instead of cmd + shift + 0
                } else {
                    KEY_NUMBER(X_0, SS_FR_RPRN) // 0 )
                }
            }
            break;

        // ----------------
        // punctuation
        case EXCLAM:
            KEY_PC_MACOS(SS_FR_EXCLAM, SS_FR_EXCLAM_MACOS) // !
        case AT:
            KEY_PC_MACOS(SS_FR_AT, SS_FR_AT_MACOS) // @
        case DASH:
            KEY_PC_MACOS(SS_FR_DASH, SS_FR_DASH_MACOS) // #
        case PERCEN:
            KEY_BASIC(SS_FR_PERCENT) // %
        case CIRC:
            KEY_BASIC(SS_FR_CIRCUMFLUX) // ^
        case AMPER:
            KEY_BASIC(SS_FR_AMPERSAND) // &
        case SD_QUOT:
            KEY_SHIFT(SS_DQUOTE, SS_SQUOTE) // " '
        case DQUOTE:
            KEY_BASIC(SS_DQUOTE) // "
        case SQUOTE:
            KEY_BASIC(SS_SQUOTE) // '
        case SCLN_CL:
            KEY_SHIFT(SS_TAP(X_DOT), SS_TAP(X_COMM)) // : ;
        case SCLN:
            KEY_BASIC(SS_TAP(X_COMM)) // ;
        case COLON:
            KEY_BASIC(SS_TAP(X_DOT)) // :
        case LPRN:
            KEY_SHIFT_PC_MACOS(SS_FR_LPRN, SS_FR_LCBR, SS_FR_LCBR_MACOS) // ( {
        case RPRN:
            KEY_SHIFT_PC_MACOS(SS_FR_RPRN, SS_FR_RCBR, SS_FR_RCBR_MACOS) // ) }
        case SLCBR:
            if (record->event.pressed) {
                // if dollar was first pressed, first LCBR is {, second in a row is }
                // if double tap to slow, only {
                if (lcbr_registered && timer_elapsed(dollar_timer) < TAPPING_TERM * 3) {
                    lcbr_registered = false;
                    KEY_PC_MACOS(SS_FR_RCBR, SS_FR_RCBR_MACOS) // }
                } else {
                    lcbr_registered = true;
                    KEY_PC_MACOS(SS_FR_LCBR, SS_FR_LCBR_MACOS) // {
                }
            }
        case LCBR:
            KEY_PC_MACOS_WITH_SHIFT(SS_FR_RCBR, SS_FR_RCBR_MACOS) // } note: if shitf is pressed, it is ]
        case RCBR:
            KEY_PC_MACOS_WITH_SHIFT(SS_FR_RCBR, SS_FR_RCBR_MACOS) // } note: if shitf is pressed, it is [
        case LBRC:
            KEY_PC_MACOS(SS_FR_LBRC, SS_FR_LBRC_MACOS) // [
        case RBRC:
            KEY_PC_MACOS(SS_FR_RBRC, SS_FR_RBRC_MACOS) // ]
        case COMMA:
            KEY_SHIFT_PC_MACOS(SS_TAP(X_M), SS_TAP(X_NUBS), SS_TAP(X_GRV)) // , <
        case DOT:
            KEY_SHIFT_PC_MACOS(SS_LSFT(SS_TAP(X_COMM)), SS_LSFT(SS_TAP(X_NUBS)), SS_LSFT(SS_TAP(X_GRV))) // . >
        case MICRO:
            KEY_PC_MACOS(SS_FR_MICRO, SS_FR_MICRO_MACOS) // µ
        case TILDE:
            KEY_PC_MACOS(SS_FR_TILD, SS_FR_TILD_MACOS SS_TAP(X_SPACE)) // ~
        case PIPE:
            KEY_PC_MACOS(SS_PIPE, SS_PIPE_MACOS) // |
        case DOLLAR:
            if (record->event.pressed) {
                dollar_timer    = timer_read();
                lcbr_registered = false;
                KEY_BASIC(SS_FR_DOLLAR) // $
            }
        case PLUS:
            KEY_PC_MACOS(SS_FR_PLUS, SS_FR_PLUS_MACOS) // +
        case EURO:
            KEY_PC_MACOS(SS_FR_EURO, SS_FR_EURO_MACOS) // €
        case ASTR:
            KEY_PC_MACOS(SS_ASTERIX, SS_ASTERIX_MACOS) // *
        case MINS:
            KEY_PC_MACOS(SS_FR_MINUS, SS_FR_MINUS_MACOS) // -
        case UNDRSCR:
            KEY_PC_MACOS(SS_UNDERSCOR, SS_UNDERSCOR_MACOS) // _
        case L_BR:
            key_shift_pc_macos(record, mod_state, SS_FR_LCBR, SS_FR_LCBR_MACOS, SS_FR_LBRC, SS_FR_LBRC_MACOS); // { [
            break;
        case R_BR:
            key_shift_pc_macos(record, mod_state, SS_FR_RCBR, SS_FR_RCBR_MACOS, SS_FR_RBRC, SS_FR_RBRC_MACOS); // } ]
            break;
        case GRA_ESC: // esc `
            if (IS_LAYER_ON(_NAV)) {
                // If already set, then switch it off
                layer_off(_NAV);
                return false;
            } else {
                KEY_SHIFT_PC_MACOS(SS_TAP(X_ESC), SS_FR_GRAVE, SS_FR_GRAVE_MACOS);
            }
            break;
        case DGRAVE: // ```
            if (record->event.pressed) {
                if (is_mac) {
                    SEND_STRING_DELAY(SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS SS_FR_GRAVE_MACOS, DELAY_TAP_CODE);
                } else {
                    SEND_STRING_DELAY(SS_FR_GRAVE SS_FR_GRAVE SS_FR_GRAVE, DELAY_TAP_CODE);
                }
            }
            break;
        case PIP_BSL: // | backslash
            if (record->event.pressed) {
                if (is_mac) {
                    KEY_SHIFT(SS_PIPE_MACOS, SS_BACKSLASH_MACOS);
                } else {
                    KEY_SHIFT(SS_PIPE, SS_BACKSLASH);
                }
            }
            break;
        case SLASH:
            if (record->event.pressed) {
                if (mod_state & MOD_MASK_SHIFT) {
                    tap_code(FR_COMM); // ?
                } else if (mod_state & MOD_MASK_GUI) {
                    tap_code16(FR_COLN); // override : cmd + / instead of cmd + :
                } else {
                    SEND_STRING_DELAY(SS_LSFT(SS_TAP(X_DOT)), DELAY_TAP_CODE); // /
                }
            }
            break;
        case UP_SLSH:
            if (record->tap.count > 0) {
                if (record->event.pressed) {
                    if (mod_state & MOD_MASK_SHIFT) {
                        tap_code(FR_COMM); // ?
                    } else if (mod_state & MOD_MASK_GUI) {
                        tap_code16(FR_COLN); // override : cmd + / instead of cmd + :
                    } else {
                        SEND_STRING(SS_LSFT(SS_TAP(X_DOT))); // /
                    }
                }
                // do not continue with default tap action
                // if the MT was pressed or released, but not held
                return false;
            }
            break;
        case ALT_LBR:
            if (record->tap.count > 0) { // Key is being tapped.
                // { [
                key_shift_pc_macos(record, mod_state, SS_FR_LCBR, SS_FR_LCBR_MACOS, SS_FR_LBRC, SS_FR_LBRC_MACOS);
            } else { // Key is being held.
                if (record->event.pressed) {
                    register_mods(MOD_MASK_LALT);
                } else {
                    unregister_mods(MOD_MASK_LALT);
                }
            }
            return false;
        case GUI_LBR:
            if (record->tap.count > 0) { // Key is being tapped.
                // { [
                key_shift_pc_macos(record, mod_state, SS_FR_LCBR, SS_FR_LCBR_MACOS, SS_FR_LBRC, SS_FR_LBRC_MACOS);
            } else { // Key is being held.
                if (record->event.pressed) {
                    register_mods(MOD_MASK_GUI);
                } else {
                    unregister_mods(MOD_MASK_GUI);
                }
            }
            return false;
        case EQUAL:
            if (record->event.pressed) {
                if (mod_gui_mac_or_ctrl_pc(mod_state)) {
                    tap_code16(KC_SLSH); // override : cmd + + instead of cmd + =
                } else {
                    // same key for PC or mac os for = +
                    if (is_mac) {
                        SEND_STRING(SS_FR_EQL_MACOS); // = +
                    } else {
                        SEND_STRING(SS_FR_EQL); // = +
                    }
                }
            }
            break;

        // ----------------
        // accent
        case A_GRAVE:
            KEY_ACCENT_CAPITAL(SS_FR_A_GRAVE, SS_FR_A_GRAVE_MACOS_CAPITAL)
        case A_CIRC:
            KEY_BASIC(SS_FR_A_CIRC)
        case A_GR_CI:
            KEY_DOUBLE_ACCENT(SS_FR_A_CIRC, SS_FR_A_GRAVE) // â à
        case E_ACUTE:
            KEY_BASIC(SS_FR_E_ACUTE)
        case E_GRAVE:
            KEY_BASIC(SS_FR_E_GRAVE)
        case E_CIRC:
            KEY_DOUBLE_ACCENT(SS_FR_E_CIRC_CAPITAL_MAC_OS, SS_FR_E_CIRC) // Ê capital circ e, ê
        case E_TREMA:
            KEY_BASIC(SS_FR_E_TREMA)
        case I_CIRC:
            KEY_BASIC(SS_FR_I_CIRC)
        case I_TREMA:
            KEY_BASIC(SS_FR_I_TREMA)
        case O_CIRC:
            KEY_BASIC(SS_FR_O_CIRC)
        case U_GRAVE:
            KEY_BASIC(SS_FR_U_GRAVE)
        case U_CIRC:
            KEY_BASIC(SS_FR_U_CIRC)
        case U_GR_CI:
            KEY_DOUBLE_ACCENT(SS_FR_U_CIRC, SS_FR_U_GRAVE) // û ù
        case U_TREMA:
            KEY_BASIC(SS_FR_U_TREMA)
        case C_CEDI:
            KEY_ACCENT_CAPITAL(SS_FR_C_CEDI, SS_FR_C_CEDI_MACOS_CAPITAL)
        case U_ACCENT:
            KEY_ACCENT_CIRC_TREMA(SS_FR_U_TREMA, SS_FR_U_CIRC)
        case I_ACCENT:
            KEY_ACCENT_CIRC_TREMA(SS_FR_I_TREMA, SS_FR_I_CIRC)
        case E_ACCENT:
            KEY_ACCENT_CIRC_TREMA(SS_FR_E_TREMA, SS_FR_E_CIRC)
        case O_ACCENT:
            KEY_ACCENT_CIRC_TREMA(SS_FR_O_TREMA, SS_FR_O_CIRC)

        // ----------------
        // utils
        case C_A_SUP:
            if (record->event.pressed) {
                SEND_STRING(SS_LALT(SS_LCTL(SS_TAP(X_DEL))));
                return false;
            }
            break;
        case MAKE:
            if (record->event.pressed) {
                if (!(mod_state & MOD_MASK_CTRL)) {
                    unregister_mods(mod_state);
#if defined(RGBLIGHT_ENABLE)
                    rgblight_show_solid_color_noeeprom(HSV_RED);
#endif
                    SEND_STRING_DELAY(SS_LCTL(SS_TAP(X_C)) "qmk flash ", DELAY_TAP_CODE);
                    send_string_with_delay(is_mac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
                    SEND_STRING_DELAY("j 6 ", DELAY_TAP_CODE);
                    send_string_with_delay(is_mac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
                    SEND_STRING_DELAY("kb " QMK_KEYBOARD " ", DELAY_TAP_CODE);
                    send_string_with_delay(is_mac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
                    SEND_STRING_DELAY("km " QMK_KEYMAP SS_TAP(X_ENTER), DELAY_TAP_CODE);
                }
                reset_keyboard();
            }
            break;
        case VERSION:
            if (record->event.pressed) {
                SEND_STRING_DELAY(QMK_KEYBOARD "/" QMK_KEYMAP " " KEYBOARD_BRANCH, DELAY_TAP_CODE);
                send_string_with_delay(is_mac ? SS_FR_AT_MACOS : SS_FR_AT, DELAY_TAP_CODE);
                SEND_STRING_DELAY(KEYBOARD_REVISION " compiled on " KEYBOARD_DATE ", layer:", DELAY_TAP_CODE);
                if (layer_state_is(_GAME_FPS)) {
                    SEND_STRING_DELAY("GAME:FPS", DELAY_TAP_CODE);
                } else if (layer_state_is(_GAME_DOOM)) {
                    SEND_STRING_DELAY("GAME:DOOM", DELAY_TAP_CODE);
                } else if (layer_state_is(_PC)) {
                    SEND_STRING_DELAY("PC", DELAY_TAP_CODE);
                } else {
                    SEND_STRING_DELAY("MAC", DELAY_TAP_CODE);
                }
                return false;
            }
            break;
        case INVALID: // xxxxxxx
            if (record->event.pressed) {
                notify_ko();
                return false;
            }
            break;
        case MUTE_BRG: {
            static bool brightness_down = true;
            if (record->event.pressed) {
                if (mod_state & MOD_MASK_SHIFT) {
                    unregister_mods(mod_state);
                    uint8_t iter = brightness_down ? 16 : 9;
                    for (uint8_t i = 0; i < iter; i++) {
                        wait_ms(DELAY_TAP_CODE);
                        tap_code(brightness_down ? KC_BRIGHTNESS_DOWN : KC_BRIGHTNESS_UP);
                    }
                    register_mods(mod_state);
                    brightness_down = !brightness_down;
                } else {
                    tap_code(KC_MUTE);
                }
            }
        } break;
            /*    case RGB_HSV:
                    // add #include <stdio.h>
                    // print current HSV values
                    if (record->event.pressed) {
                        SEND_STRING("HSV: ");
                        char var[10];
                        sprintf(var, "%i", rgblight_get_hue());
                        send_string(var);
                        SEND_STRING(",");
                        sprintf(var, "%i", rgblight_get_sat());
                        send_string(var);
                        SEND_STRING(",");
                        sprintf(var, "%i", rgblight_get_val());
                        send_string(var);
                        SEND_STRING(SS_TAP(X_ENTER));
                    }
                    break; */

        // ----------------
        // navigation
        case HOME:
            KEY_PC_MACOS_WITH_SHIFT(SS_TAP(X_HOME), SS_LGUI(SS_TAP(X_LEFT)))
        case END:
            KEY_PC_MACOS_WITH_SHIFT(SS_TAP(X_END), SS_LGUI(SS_TAP(X_RIGHT)))

        // ----------------
        // super charged keys
        case ALT_TAB:
            // for PC: map ctrl+tab on alt+tab
            if (record->event.pressed) {
                // Detect the activation of either ctrl keys
                if (mod_state & MOD_MASK_CTRL) {
                    // First temporarily canceling both ctrl so that
                    // alt isn't applied to the KC_TAB keycode
                    unregister_mods(MOD_MASK_CTRL);
                    wait_ms(DELAY_TAP_CODE);
                    register_mods(MOD_MASK_ALT);
                    // Update the boolean variable to reflect the status of KC_TAB
                    tabkey_registered = true;
                }
                register_code(KC_TAB);
            } else {
                unregister_code(KC_TAB);
            }
            break;
        case MAGIC_T:
            // - for easier access instead of alt+tab: map ctrl+T on lgui+tab for mac, ctrl+T on alt+tab for PC
            if (record->event.pressed) {
                // common function
                bool start_tab(uint8_t mod_to_deactivate, uint8_t mod_to_activate) {
                    if (mod_state & mod_to_deactivate) {
                        // First temporarily canceling mod so that
                        // alt isn't applied to the KC_TAB keycode
                        unregister_mods(mod_to_deactivate);
                        wait_ms(DELAY_TAP_CODE);
                        register_mods(mod_to_activate);
                        // Update the boolean variable to reflect the status of KC_TAB
                        tabkey_registered = true;
                        register_code(KC_TAB);
                        return false;
                    } else if (tabkey_registered) {
                        register_code(KC_TAB);
                        return false;
                    }
                    return true;
                }
                bool res = false;
                if (is_mac) {
                    res = start_tab(MOD_MASK_CTRL, MOD_MASK_GUI);
                } else {
                    res = start_tab(MOD_MASK_CTRL, MOD_BIT(KC_LEFT_ALT));
                }
                if (!res) {
                    return false;
                }
                // Tab is not registered, continue
                register_code(FR_T);
            } else {
                if (tabkey_registered) {
                    unregister_code(KC_TAB);
                } else {
                    unregister_code(FR_T);
                }
            }
            break;
        case MAGIC_G:
            // for mac only: map ctrl+G on lgui+esc for easier access
            if (record->event.pressed) {
                // Detect the activation of either ctrl keys
                if (is_mac) {
                    if (mod_state & MOD_MASK_CTRL) {
                        // First temporarily canceling both ctrl so that
                        // alt isn't applied to the KC_TAB keycode
                        unregister_mods(MOD_MASK_CTRL);
                        wait_ms(DELAY_TAP_CODE);
                        register_mods(MOD_MASK_GUI);
                        // Update the boolean variable to reflect the status of KC_TAB
                        tabkey_registered = true;
                        register_code(KC_NUBS);
                        return false;
                    } else if (tabkey_registered) {
                        register_code(KC_NUBS);
                        return false;
                    }
                }
                // Tab is not registered, continue
                register_code(FR_G);
            } else {
                if (tabkey_registered) {
                    unregister_code(KC_NUBS);
                } else {
                    unregister_code(FR_G);
                }
            }
            break;
        case KC_LCTL:
        case KC_RCTL:
            // used with alt + tab above
            if (!record->event.pressed && tabkey_registered) {
                tabkey_registered = false;
                unregister_mods(MOD_MASK_GUI | MOD_MASK_ALT | MOD_MASK_CTRL);
                return false;
            }
            break;
        case MAGIC_BC: {
            // - if shift is registered, send backspace instead.
            // - with ctrl, send key up instead
            static bool delkey_registered;
            if (record->event.pressed) {
                // Detect the activation of either shift keys
                if (mod_state & MOD_MASK_SHIFT) {
                    // First temporarily canceling both shifts so that
                    // shift isn't applied to the KC_DEL keycode
                    unregister_mods(MOD_MASK_SHIFT);
                    wait_ms(DELAY_TAP_CODE);
                    if (shift_registered > 1) { // if shift is registered, send backspace instead.
                        register_code(KC_BSPC);
                    } else {
                        register_code(KC_DEL);
                    }
                    // Update the boolean variable to reflect the status of KC_DEL
                    delkey_registered = true;
                    // Reapplying modifier state so that the held shift key(s)
                    // still work even after having tapped the Backspace/Delete key.
                    set_mods(mod_state);
                } else if (mod_state & MOD_MASK_CTRL) {
                    // map to key up
                    unregister_mods(MOD_MASK_CTRL);
                    wait_ms(DELAY_TAP_CODE);
                    tap_code(KC_UP);
                    set_mods(mod_state);
                } else {
                    register_code(KC_BSPC);
                }
            } else {
                // In case KC_DEL is still being sent even after the release of KC_BSPC
                if (delkey_registered) {
                    if (shift_registered > 1) {
                        unregister_code(KC_BSPC);
                    } else {
                        unregister_code(KC_DEL);
                    }
                    delkey_registered = false;
                } else {
                    unregister_code(KC_BSPC);
                }
            }
        } break;
        case MAGIC_W:
            if (previous_key_number) {
                if (record->event.pressed) {
                    SEND_STRING(SS_LSFT(SS_TAP(X_COMM))); // .
                }
            } else {
                if (record->event.pressed) {
                    register_code(KC_Z);
                } else {
                    unregister_code(KC_Z);
                }
            }
            break;

        // ----------------
        // layers
        case PC:
            if (record->event.pressed) {
                switch_layer_to_pc();
            }
            return false;
        case MAC:
            if (record->event.pressed) {
                switch_layer_to_mac();
            }
            return false;
        case G_FPS:
            if (record->event.pressed) {
                switch_layer_to_game(_GAME_FPS, true);
            }
            return false;
        case G_DOOM:
            if (record->event.pressed) {
                switch_layer_to_game(_GAME_DOOM, false);
            }
            return false;
        case MO(_FUNC): {
#if defined(RGBLIGHT_ENABLE)
            static uint16_t fn_layer_timer = 0;
            if (record->event.pressed) {
                fn_layer_timer = timer_read();
            } else {
                // key released
                if (timer_elapsed(fn_layer_timer) < TAPPING_TERM) {
                    notify_color_layer();
                }
            }
#endif
        } break;
    } // end switch(keycode)

    // Let QMK process the keycode as usual
    return true;
}

/**************** Tap dance ****************/

void dance_smile_happy(tap_dance_state_t *state, void *user_data) {
    uint8_t mod_state = get_mods();
    bool    smile     = true;
    if ((!(mod_state & MOD_MASK_SHIFT) && timer_elapsed(shift_timer) < (get_tapping_term(SP_RSFT, NULL) + 100)) || mod_state & MOD_MASK_SHIFT) {
        // shift was released during the tap dance
        smile = false;
        unregister_mods(MOD_MASK_SHIFT);
        del_mods(MOD_MASK_SHIFT);
        del_weak_mods(MOD_MASK_SHIFT);
    }

    if (smile) {
        if (state->count == 1) { // :-)
            SEND_STRING_DELAY(":", DELAY_TAP_CODE);
            send_string_with_delay(is_mac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
            SEND_STRING(") ");
        } else if (state->count == 2) { // ;-)
            SEND_STRING_DELAY(";", DELAY_TAP_CODE);
            send_string_with_delay(is_mac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
            SEND_STRING_DELAY(") ", DELAY_TAP_CODE);
        } else if (state->count >= 3) { // :muscle:
            SEND_STRING_DELAY(":muscle: ", DELAY_TAP_CODE);
        }
    } else {
        if (state->count == 1) { // :-(
            SEND_STRING_DELAY(":", DELAY_TAP_CODE);
            send_string_with_delay(is_mac ? SS_FR_MINUS_MACOS : SS_FR_MINUS, DELAY_TAP_CODE);
            SEND_STRING_DELAY("( ", DELAY_TAP_CODE);
        } else if (state->count == 2) { // :sob:
            SEND_STRING_DELAY(":sob: ", DELAY_TAP_CODE);
        } else if (state->count >= 3) { // :scream:
            SEND_STRING_DELAY(":scream: ", DELAY_TAP_CODE);
        }
    }
    reset_tap_dance(state);
}

tap_dance_action_t tap_dance_actions[] = {
    [SMILEY_HAPPY] = ACTION_TAP_DANCE_FN(dance_smile_happy),
};

/**************** Leader key ****************/

void leader_start_user(void) {
#if defined(RGBLIGHT_ENABLE)
    store_rgb_mode();
    rgblight_show_solid_color_noeeprom(HSV_SPRINGGREEN);
#endif
}

void leader_end_user(void) {
    bool did_leader_succeed = false;

    if (leader_sequence_one_key(FR_S)) {
        // Intelli J > file structure
        tap_code16(LGUI(KC_F12));
        did_leader_succeed = true;
    } else if (leader_sequence_one_key(FR_M)) {
        SEND_STRING_DELAY("Merci d'avance et bonne journ", DELAY_TAP_CODE);
        tap_code16(FR_EACU);
        wait_ms(DELAY_TAP_CODE);
        SEND_STRING("e");
        did_leader_succeed = true;
    } else if (leader_sequence_one_key(FR_B)) {
        SEND_STRING_DELAY("Bonne journ", DELAY_TAP_CODE);
        tap_code16(FR_EACU);
        wait_ms(DELAY_TAP_CODE);
        SEND_STRING("e");
        did_leader_succeed = true;
    } else if (leader_sequence_one_key(FR_G)) {
        SEND_STRING_DELAY("Belle journ", DELAY_TAP_CODE);
        tap_code16(FR_EACU);
        wait_ms(DELAY_TAP_CODE);
        SEND_STRING("e");
        did_leader_succeed = true;
    }

    if (did_leader_succeed) {
#if defined(RGBLIGHT_ENABLE)
        restore_previous_rgb_mode();
#endif
    } else {
#if defined(RGBLIGHT_ENABLE)
        restore_previous_rgb_mode();
        notify_color(HSV_RED);
#endif
    }
}

/**************** Encoder ****************/

bool encoder_update_user(uint8_t index, bool clockwise) {
    uint8_t mod_state = get_mods();
#ifdef KEYBOARD_dactyl_flactyl
    if (index == 0) {
        // left hand
        if (clockwise) {
            tap_code(KC_MS_WH_UP);
        } else {
            tap_code(KC_MS_WH_DOWN);
        }
    } else {
#endif
        // right hand
        if (mod_state & MOD_MASK_SHIFT) {
            unregister_mods(mod_state);
            wait_ms(DELAY_TAP_CODE);
            if (clockwise) {
                tap_code(KC_BRIGHTNESS_UP);
            } else {
                tap_code(KC_BRIGHTNESS_DOWN);
            }
            register_mods(mod_state);
        } else if (mod_gui_mac_or_ctrl_pc(mod_state)) {
            if (clockwise) {
                tap_code16(KC_SLSH); // override : cmd + + instead of cmd + =
            } else {
                tap_code(FR_EQL);
            }
        } else {
            if (clockwise) {
                tap_code(KC_AUDIO_VOL_UP);
            } else {
                tap_code(KC_AUDIO_VOL_DOWN);
            }
        }
#ifdef KEYBOARD_dactyl_flactyl
    }
#endif
    return false;
}

/**************** Tapping term ****************/

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case TD_HAPP:
            return 350;
        case SP_RSFT:
        case SP_LSFT:
            return 200;
        default:
            return TAPPING_TERM;
    }
}

/* repetition for dual role keys */
uint16_t get_quick_tap_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case SP_RSFT:
            return 0;
        default:
            return QUICK_TAP_TERM;
    }
}

/**************** Keyboard init ****************/

void keyboard_post_init_user(void) {
    if (is_keyboard_master()) {
        os_variant_t os_variant = detected_host_os();

        switch (os_variant) {
            case OS_LINUX:
            case OS_WINDOWS:
                switch_layer_to_pc();
                break;
            default:
                switch_layer_to_mac();
                break;
        }
#if defined(RGBLIGHT_ENABLE)
        is_notify_color_layer = true;
        velocikey_toggle();
        rgblight_enable_noeeprom();
#endif
    }
}
