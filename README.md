# Dactyl-manuform ⌨️

A handwired *Dactyl* manuform keyboard with 7x4 keys and 5 thumb keys and a flat version of the case.

Hardware features:
  - [STM32F401 microcontroller](https://github.com/WeActStudio/WeActStudio.MiniSTM32F4x1) (also known as the BlackPill)
  - 1 or 2 rotary encoder
  - 3D-printed case
  - Hot-swappable switch
  - Serial communication between the halves using a RJ11 or TRRS cable
  - USB-C magnetic connector
  - Only 1 U keycaps
  - RGB LED on each side

### Project structure

* **/case** - `stl`, `json`, `3mf`, `openscad` and `fusion 360` files
* **/qmk** - Firmware folder for QMK
* **/doc** - Pictures, Json files used for [keyboard-layout-editor.com](http://www.keyboard-layout-editor.com/) (TBD)

## 3D Case 🔧

Two cases are available: the original *Dactyl*: high and ergonomic and the *Flactyl*: low and transportable.

### Dactyl

![Top view](doc/dactyl-top.jpg)

![Back view](doc/dactyl-back.jpg)

The case was generated using [a web generator hosted on this website](https://dactyl.mbugert.de/) and with the help of [this documentation to see the effect of each setting](https://github.com/yejianfengblue/dactyl-generator-demo). I [modified the `scad` file](case/manuform-case-right.scad) to:
- added 1 screw insert
- moved all screw inserts inside the body to hide them
- widen the rectangular hole for the external holder

All stl files are described below in the [3d printed parts chapter](#3d-printed-parts).

![openscad](doc/openscad.jpg)

[Here is the json](case/manuform-any.json) used to re-import all the settings in the generator.

I used Ultimaker Cura to slice the model. I printed it at 0.28 mm layer height with 'support tree' support and brim. The print time was about 18 hours long.

The bottom plate was designed with Fusion 360 and heavily inspired of my [Agathe keyboard](https://gitlab.com/coliss86/agathe):

![Bottom view](doc/dactyl-bottom.jpg)

As a replacement for the original holder that contains the microcontroller and the TRSS socket, I designed a new one using a RJ11 socket and a USB-C PCB breakout with a magnetic connector. It is higher than the original holder, so I modified the corresponding hole in the `scad` file to fit it.

![external holder](doc/external_holder_left.jpg)

The rectangular hole fits the RJ11 socket, and the round one fits the USB-C magnetic connector and the USB-C PCB breakout.

Here is the printed version:

![external holder](doc/external_holder.jpg)

And here it is with the sockets mounted:

![external holder sockets](doc/external_holder_sockets.jpg)

### Flactyl

The *Dactyl* is a portable keyboard but does not easily fit in a bag. So, I created a variant that is almost flat: same microcontroller, same layout, same wiring &rArr; same firmware. I named it *Flactyl*: it is a **flat Dactyl**.
It features a higher number of led on the bottom.

The Fusion 360 files for the case is located in the [3d-models/flactyl folder](3d-models/flactyl/v2/). The design is based on the [Reredox design](https://www.printables.com/model/383665-reredox-redesign-of-a-redesign).
Fusion 360 files for all parts I designed are available if modifications are needed.

Here are some pictures of it:

![top](doc/flactyl-top.jpg)

![bottom](doc/flactyl-bottom.jpg)

## Electronic 📟

The micro-controller used is STM32F401, the pinout can [be found here](https://raw.githubusercontent.com/WeActStudio/WeActStudio.MiniSTM32F4x1/master/images/STM32F4x1_PinoutDiagram_RichardBalint.png), but not all pins are usable. The [QMK documentation explains it here](https://docs.qmk.fm/platformdev_blackpill_f4x1).

- Two sets of wiring connections for rows and columns are used to maximise the available space inside the case: all the cables are located as far as possible from the back plate:
  - Right:
    - Columns: `B12`, `B13`, `B14`, `B15`, `A15`, `B3`, `B4`
    - Rows: `B9`, `B8`, `B7`, `B6`, `B5`
  - Left:
    - Columns: `A4`, `A5`, `A6`, `A7`, `B0`, `B1`, `B10`
    - Rows: `C13`, `C14`, `C15`, `A0`, `A1`

- The handedness by pin is done with a resistor of 1 kΩ between `A13` and `GND` or `VCC` : if it is connected to `VCC`, it is the left side, right side otherwise.
- Communication between the 2 halves is done with a serial communication running in [full duplex](https://docs.qmk.fm/drivers/serial#usart-full-duplex) with the pins `A2` and `A3`.
- The RGB LED is connected to `A8` on both side.

![wiring](doc/wiring.png)

The inside of the keyboard:

![wiring](doc/dactyl-wiring.jpg)

Hand wiring a keyboard with so many switches is time-consuming, but not difficult. Here is a [guide I followed](https://arnmk.com/building-a-dactyl-manuform-with-hot-swappable-sockets/) ([direct link to the wiring diagram](https://arnmk.com/content/images/2020/09/Wiring-Diagram-1.svg)).

For the flactyl, I used a PCB per key Altena PCB: it is a [Kailh socket mounted on a PCB with per key underglow led](https://github.com/swanmatch/MxLEDBitPCB/tree/master/altana).

![bottom](doc/flactyl-inside.jpg)

The thumb cluster and the last keys on the bottom of the 2 center columns rows are wired as a singel row. On the picture below, the red lines show the connections with the columns and the yellow ones indicate a cut in the PCB:

![pcb](doc/flactyl-last-row.jpg)

## LEDs 🚦

LEDs are used to display the keyboard's status. They are located on each side at the bottom.

Here are the functionalities:
- The color changes with each keypress within the rainbow color range.
- When pressing an invalid key or an invalid combination, they flash **red**.
- When changing the layout using the `Fn` key, the LEDs are **white** for the `_MACOS` layer and **blue** for the `_PC` layer.
- When _[Caps Word](https://docs.qmk.fm/features/caps_word)_ is activated, they are **yellow** as long as it is on.

## Layout 👗👔

I use [COLEMAK mod DH](https://colemakmods.github.io/mod-dh/) as a layout and have moved some keys to place the most frequently used French accents on the base layer, such as <kbd>é</kbd> in the place of <kbd>y</kbd> and <kbd>à</kbd> in the place of <kbd>w</kbd>.

![colemak mod dh with french accent](doc/keyboard-layout.png)

Notes: the colours are corresponding to the colours of the keycaps I'm using, they have no particular signification.

### But why Azerty keycodes ⁉️

This keyboard is intended to be used as a replacement for an Azerty keyboard on French computers without any modifications on the OS side.

I use a Macbook with an integrated Azerty keyboard. I often connect an external PC keyboard to it, but OS X does not map it to a PC layout without any action. You have to go to `System Preferences` > `Keyboard` > `Input Sources` > `Add the layout "French PC"`. Then, every time I want to use the integrated or external keyboard, I have to change the layout. It is a shame that OS X is not able to handle a layout per keyboard.
Thus, this keyboard behave like any Azerty Mac keyboard in order to not require any change on the OS part.

### Features 🔮

Special features of this layout:
- [Tap dance](https://docs.qmk.fm/features/tap_dance): one tap: `:-)`, two taps: `;-)`, three taps: `:muscle:`...
- A rotary encoder is used to adjust the sound volume and combined with <kbd>shift</kbd>, it adjusts the screen brightness
- 2 RGB LEDs, one on each side on the bottom of the center column, change color depending on action: off by default, yellow when [caps word](https://docs.qmk.fm/features/caps_word) is activated, flash red when a wrong combo is typed etc...
- Workaround for Ubuntu 21.04 which doesn't listen to keys pressed too fast, such as pressing <kbd>shift + 1</kbd> and then releasing <kbd>shift</kbd>

## BOM 🧾

### Hardware

- [ ] 68 * Cherry MX switches, 34 keys per hand
- [ ] 68 * Hot-swappable PCB sockets
- [ ] 68 * 1 U keycaps, I recommend XDA or DSA for easier moving of keycaps
- [ ] 68 * 1N4148 diode
- [ ] 10 * M3 screw insert
- [ ] 10 * M3x16 screw
- [ ] 5 * M3x6 screw (*Flactyl* only)
- [ ] 2 * STM32F401CCU6 3.0 development board
- [ ] 2 * Resistor 1 kΩ
- [ ] 2 * RJ11 telephone female connector
- [ ] 2 * USB-C PCB breakout board
- [ ] 2 * SK6812 RGB LED
- [ ] 1 * Male RJ11 To male RJ11 telephone cord curly cable (*Dactyl* only)
- [ ] 1 * Male to Male TRRS cable (*Flactyl* only)
- [ ] 1 * USB-C male to male cable
- [ ] 1 * EC11 encoder with knob

### 3D printed parts

#### Dactyl

The Stl and 3mf files for the *Dactyl* case are available in [the 3d-models/dactyl folder](3d-models/dactyl/).
Here is the print list:
- [ ] 1 * [Right half](3d-models/dactyl/manuform-case-right.stl), mirror it for the left hand side
- [ ] 1 * [External holder right](3d-models/dactyl/external-holder-right.stl)
- [ ] 1 * [External holder left](3d-models/dactyl/external-holder-left.stl)
- [ ] 2 * Bottom: I have designed 2 versions of the bottom plate:
  - A simple one with honeycomb holes, print the [bottom right](3d-models/dactyl/bottom.stl) and mirror it for the left hand side
  - An enhanced one with a pad for a wrist rest, connected to the keyboard to ensure the distance between the 2 parts. For each hands, 3 parts need to be printed:
    - [ ] 1 * [Bottom right](3d-models/dactyl/bottom-with-pad.stl), mirror it for the left hand side
    - [ ] 1 * [Connector](3d-models/dactyl/connector-55.stl)
    - [ ] 1 * [Pad for the wrist rest](3d-models/dactyl/pad.stl)

#### Flactyl

The files are in the [the 3d-models/flactyl folder](3d-models/flactyl/v2/):
- [ ] 1 * [Right half](3d-models/flactyl/v2/top.stl), just mirror it for the left hand side.
- [ ] 1 * [Bottom](3d-models/flactyl/v2/bottom.stl)

### Printing settings ⚙️

For the *Dactyl*, the piece is oriented with the keys facing up, supports are needed. I chose 'tree supports' in Cura as it is easy to remove.

![print settings](doc/print-settings.jpg)

Here are the settings I used:

![print settings](doc/print-settings-2.jpg)

## Useful links 🔗

- [Building guide](https://arnmk.com/building-a-dactyl-manuform-with-hot-swappable-sockets/)
- [Split doc](https://github.com/qmk/qmk_firmware/blob/master/docs/feature_split_keyboard.md)
- [Pinout of STM32F401](https://raw.githubusercontent.com/WeActStudio/WeActStudio.MiniSTM32F4x1/master/images/STM32F4x1_PinoutDiagram_RichardBalint.png)
- [keymap tractyl_manuform by @drashna](https://github.com/qmk/qmk_firmware/tree/master/keyboards/handwired/tractyl_manuform/5x6_right/f411)
- [tap and hold](https://getreuer.info/posts/keyboards/triggers/index.html)
